/*
Folder and Files Preparation: 

1. Create a "readingListAct" Folder in your Batch Folder. 
2. Create an index.html and index.js file. For the index.html title just write "Reading List Activity 1" 
3. Show your solutions for each problem in the index.js file. 
4. Once done, Create a remote repository named "readingListAct".
5. Save your repo link in Boodle: WDC028V1.5b-18-C1 | JavaScript - Function Parameters, Return Statement and Array Manipulations


*/

// Activity Template:
// (Copy this part on your template)

/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:
function setInAlphabeticalOrder(string) {
  let tester = "abcdefghijklmnopqrstuvwxyz";
  let alphabeticalOrder = "";

  for (let x = 0; x < tester.length; x++) {
    for (let y = 0; y < string.length; y++) {
      if (string[y].toLowerCase() === tester[x].toLowerCase()) {
        alphabeticalOrder += string[y];
      }
    }
  }

  return alphabeticalOrder;
}
let returnedString = setInAlphabeticalOrder("mastermind");
console.log(returnedString);

/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

// Code here:
console.log("");
myColor = ["Red", "Green", "White", "Black"];

const myColorString = `${myColor.toString()}
${myColor.toString()}
${myColor.join("+")}`;
console.log(myColorString);

/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:
console.log("");
let myGift;
function birthdayGift(gift) {
  let message;
  if (gift === "stuffed toy") {
    message = `Thank you for the ${gift}, Michael!`;
  } else if (gift === "doll") {
    message = `Thank you for the ${gift}, Sarah!`;
  } else if (gift === "cake") {
    message = `Thank you for the ${gift}, Donna!`;
  } else {
    message = `Thank you for the ${gift}, Dad!`;
  }

  return message;
}

myGift = birthdayGift("stuffed toy");
console.log(myGift);
myGift = birthdayGift("doll");
console.log(myGift);
myGift = birthdayGift("cake");
console.log(myGift);
myGift = birthdayGift("car");
console.log(myGift);

/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:
console.log("");
function countString(string) {
  let y = 0;
  for (let x = 0; x < string.length; x++) {
    if (
      string[x].toLowerCase() === "a" ||
      string[x].toLowerCase() === "e" ||
      string[x].toLowerCase() === "i" ||
      string[x].toLowerCase() === "o" ||
      string[x].toLowerCase() === "u"
    ) {
      y++;
    }
  }
  console.log(y);
}

countString("the quick brown fox");
